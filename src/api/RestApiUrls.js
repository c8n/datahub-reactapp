export const DATA_SERVICE = 'http://localhost:8091';

export const QUERY = '/api/v1/query';

export const COLLECTION_CREATE = '/api/v1/collection/save';
export const COLLECTION_DELETE = '/api/v1/collection/delete';
export const COLLECTION_GETALLNAMES = '/api/v1/collection/getallnames';

export const ROW_CREATE = '/api/v1/row/save';
export const ROW_DELETE = '/api/v1/row/delete';
export const ROW_FINDBYID = '/api/v1/row/findbyid';
export const ROW_GETALL = '/api/v1/row/findall';
export const ROW_UPDATE = '/api/v1/row/update';

export const COL_GETALL = '/api/v1/row/getallcolumnnames';
export const COL_CREATE = '/api/v1/row/addcolumns';